package com.salud.merkeapp.api;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiRest {

    public static Retrofit getInstance() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://5da08e004d823c0014dd3bf8.mockapi.io/api/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit;
    }

}
