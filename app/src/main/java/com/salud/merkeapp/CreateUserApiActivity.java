package com.salud.merkeapp;


import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;


import com.sofka.jovenes_creativos.models.User;
import com.salud.merkeapp.api.UsersService;
import com.salud.merkeapp.api.ApiRest;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateUserApiActivity extends AppCompatActivity {

    private EditText etName;
    private EditText etLastName;
    private EditText etEmail;
    private EditText etNumber;
    private Button btnSave;
    private ProgressBar progressBar;

    private UsersService usersService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        usersService = ApiRest.getInstance().create(UsersService.class);
        loadViews();
        setupOnClicks();
        getUsers();
    }

    private void getUsers() {
        Call<List<User>> userCall = usersService.getUsers();
        userCall.enqueue(new Callback<List<User>>() {
            @Override
            public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                List<User> users = response.body();
                Toast.makeText(CreateUserApiActivity.this, "Cantidad usuarios: " + users.size(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<List<User>> call, Throwable t) {
                Toast.makeText(CreateUserApiActivity.this, "Ha ocurrido un error, intente de nuevo", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void loadViews() {
        etName = findViewById(R.id.et_name);
        etLastName = findViewById(R.id.et_lastname);
        etEmail = findViewById(R.id.et_email);
        etNumber = findViewById(R.id.et_number);
        btnSave = findViewById(R.id.btn_save);
        progressBar = findViewById(R.id.progress_bar);
    }

    private void setupOnClicks() {
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                save();
            }
        });
    }

    private void save() {
        String name = etName.getText().toString();
        String lastName = etLastName.getText().toString();
        String email = etEmail.getText().toString();
        String number = etNumber.getText().toString();
        User user = new User(null, name, lastName, email, number);

        progressBar.setVisibility(View.VISIBLE);
        Call<User> userCall = usersService.createUser(user);
        userCall.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                User userResponse = response.body();
                Toast.makeText(CreateUserApiActivity.this, "Usuario creado exitosamente id: " + userResponse.getId(), Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Toast.makeText(CreateUserApiActivity.this, "Ha ocurrido un error, intente de nuevo", Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.GONE);
            }
        });
    }
}
